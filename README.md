# README #

SpringBoot app which simulates a Service Broker Provider storing state in an in-memory HSQL database
[OSB API](https://github.com/openservicebrokerapi/servicebroker)

### How do I get set up? ###

* Start SpringBoot application
* Look for the random basic auth password generated during startup, the user is "user"
* Check the hardcoded [Service Catalog](http://localhost:8080/v2/catalog)
* Request a service instance:

http://127.0.0.1:8080/v2/service_instances/my_desired_instance

~~~~ 
{
  "service_id": "default-service",
  "plan_id": "default-plan",
  "organization_guid": "atlassian",
  "space_guid": "jira",
  "parameters": {
    "user": "my-user",
    "env": "dev"
  }
}
~~~~

