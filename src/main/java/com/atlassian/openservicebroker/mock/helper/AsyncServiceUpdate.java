package com.atlassian.openservicebroker.mock.helper;

import com.atlassian.openservicebroker.mock.model.ServiceInstance;
import com.atlassian.openservicebroker.mock.model.ServiceInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncServiceUpdate {

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Async
    public void updateServiceInstance(ServiceInstance si, String operation, Long msecs) {
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        si.setLastOperation(operation);
        serviceInstanceRepository.save(si);
    }

}
