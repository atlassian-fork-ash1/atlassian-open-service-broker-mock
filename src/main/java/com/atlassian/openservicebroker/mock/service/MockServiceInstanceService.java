package com.atlassian.openservicebroker.mock.service;

import com.atlassian.openservicebroker.mock.model.ServiceInstance;
import com.atlassian.openservicebroker.mock.model.ServiceInstanceRepository;
import com.atlassian.openservicebroker.mock.service.creator.MockServiceCreator;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.servicebroker.model.*;
import org.springframework.cloud.servicebroker.service.ServiceInstanceService;
import org.springframework.stereotype.Service;

@Service
public class MockServiceInstanceService implements ServiceInstanceService {

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Autowired
    BeanFactory bf;

    @Override
    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest request) {

        ServiceInstance si = serviceInstanceRepository.findByInstanceId(request.getServiceInstanceId());

        if (si != null) {
            return new CreateServiceInstanceResponse()
                    .withAsync(false)
                    .withDashboardUrl(String.format("http://dashboard.url?instanceId=%s", si.getId()))
                    .withInstanceExisted(true);
        }

        return ((MockServiceCreator)bf.getBean(request.getServiceDefinitionId())).createServiceInstance(request);

    }

    @Override
    public GetLastServiceOperationResponse getLastOperation(GetLastServiceOperationRequest request) {

        ServiceInstance si = serviceInstanceRepository.findByInstanceId(request.getServiceInstanceId());
        OperationState state = OperationState.IN_PROGRESS;

        if (si.getLastOperation().equals("created")) {
                state = OperationState.SUCCEEDED;
        }

        return new GetLastServiceOperationResponse()
            .withDescription(si.getLastOperation())
            .withOperationState(state);

    }

    @Override
    public DeleteServiceInstanceResponse deleteServiceInstance(DeleteServiceInstanceRequest request) {
        ServiceInstance si = serviceInstanceRepository.findByInstanceId(request.getServiceInstanceId());
        serviceInstanceRepository.delete(si.getId());
        return new DeleteServiceInstanceResponse().withAsync(false);

    }

    @Override
    public UpdateServiceInstanceResponse updateServiceInstance(UpdateServiceInstanceRequest request) {
        return new UpdateServiceInstanceResponse().withAsync(false);

    }
}
