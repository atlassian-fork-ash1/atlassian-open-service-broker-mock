package com.atlassian.openservicebroker.mock.service.creator;

import org.springframework.cloud.servicebroker.model.CreateServiceInstanceRequest;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceResponse;
import org.springframework.stereotype.Component;

@Component
public interface MockServiceCreator {

    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest request);

}
