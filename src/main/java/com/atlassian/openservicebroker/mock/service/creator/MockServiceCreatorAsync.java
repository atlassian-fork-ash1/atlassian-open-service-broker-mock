package com.atlassian.openservicebroker.mock.service.creator;

import com.atlassian.openservicebroker.mock.helper.AsyncServiceUpdate;
import com.atlassian.openservicebroker.mock.model.ServiceInstance;
import com.atlassian.openservicebroker.mock.model.ServiceInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceRequest;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceResponse;
import org.springframework.stereotype.Component;

@Component("service-async")
public class MockServiceCreatorAsync implements MockServiceCreator {

    Logger logger = LoggerFactory.getLogger(MockServiceCreatorAsync.class);

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Autowired
    AsyncServiceUpdate asyncServiceUpdate;

    @Override
    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest request) {
        logger.info("Creating service " + request.getServiceInstanceId());
        ServiceInstance si = ServiceInstance.requestToBean(request);
        si.setLastOperation("scheduled");
        si = serviceInstanceRepository.save(si);
        asyncServiceUpdate.updateServiceInstance(si, "created", 10000L);
        return new CreateServiceInstanceResponse()
                .withAsync(true)
                .withDashboardUrl(String.format("http://dashboard.url?instanceId=%s", si.getId()))
                .withInstanceExisted(false);
    }
}
