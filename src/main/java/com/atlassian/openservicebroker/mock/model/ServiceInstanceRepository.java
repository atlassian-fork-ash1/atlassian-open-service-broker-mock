package com.atlassian.openservicebroker.mock.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceInstanceRepository extends CrudRepository<ServiceInstance, Long> {

    @Override
    <S extends ServiceInstance> S save(S entity);

    @Override
    ServiceInstance findOne(Long aLong);

    @Override
    boolean exists(Long aLong);

    @Override
    void delete(Long aLong);

    ServiceInstance findByInstanceId(String instanceId);

}
